---
---

**Contact:**

![E-mail](/images/icon-mail.png) [contacte@mrtraduccions.cat](mailto:contacte@mrtraduccions.cat)

![Phone:](/images/icon-phone.png) (+34) 652 492 008

&nbsp;

![Twitter:](/images/icon-twitter.png) [@marcrierai](https://www.twitter.com/marcrierai)

![LinkedIn:](/images/icon-linkedin.png) [Marc Riera Irigoyen](https://www.linkedin.com/in/marc-riera-irigoyen/)
